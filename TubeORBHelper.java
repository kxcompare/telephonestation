import Cell.Station;
import Cell.StationHelper;
import Cell.TubeCallback;
import Cell.TubeCallbackHelper;
import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContext;
import org.omg.CosNaming.NamingContextHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

class ORBThread extends Thread {
    private ORB myOrb;

    public ORBThread(ORB orb) {
        myOrb = orb;
    }

    @Override
    public void run() {
        myOrb.run();
    }
}

public class TubeORBHelper {

    private String[] args;
    private Tube tube;
    private TubeCallback callback;

    public TubeORBHelper(String[] args, Tube tube) {
        this.args = args;
        this.tube = tube;
    }

    public Station processTube() throws org.omg.CORBA.UserException {
        ORB orb = ORB.init(args, null);
        POA rootPOA = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
        rootPOA.the_POAManager().activate();

        TubeCallbackServant listener = tube.getServant();
        rootPOA.activate_object(listener);

        this.callback = TubeCallbackHelper.narrow(rootPOA.servant_to_reference(listener));

        // Получение контекста именования
        org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
        NamingContext ncRef = NamingContextHelper.narrow(objRef);

        // Преобразование имени базовой станции в объектную ссылку
        NameComponent nc = new NameComponent("BaseStation", "");
        NameComponent path[] = {nc};
        Station stationRef = StationHelper.narrow(ncRef.resolve(path));

        // Регистрация трубки в базовой станции
//        stationRef.register(ref, tube.getNumber());
//        System.out.println("Трубка зарегистрирована базовой станцией");

        // Запуск ORB в отдельном потоке управления
        // для прослушивания вызовов трубки
        ORBThread orbThr = new ORBThread(orb);
        orbThr.start();
        return stationRef;
    }

    public TubeCallback getCallback() {
        return callback;
    }
}
