import Cell.Station;
import Cell.StationHelper;
import org.omg.CORBA.ORB;
import org.omg.CosNaming.NameComponent;
import org.omg.CosNaming.NamingContextExt;
import org.omg.CosNaming.NamingContextExtHelper;
import org.omg.PortableServer.POA;
import org.omg.PortableServer.POAHelper;

public class StationORBHelper {

    private ORB orb;
    private String[] args;

    public StationORBHelper(String[] args) {
        this.args = args;
    }

    public void processStation() throws org.omg.CORBA.UserException {
        ORB orb = ORB.init(args, null);


        POA rootPOA = POAHelper.narrow(orb.resolve_initial_references("RootPOA"));
        rootPOA.the_POAManager().activate();


        StationServant servant = new StationServant();


        org.omg.CORBA.Object ref = rootPOA.servant_to_reference(servant);
        Station stationRef = StationHelper.narrow(ref);

        org.omg.CORBA.Object objRef = orb.resolve_initial_references("NameService");
        NamingContextExt ncRef = NamingContextExtHelper.narrow(objRef);


        String name = "BaseStation";
        NameComponent[] path = ncRef.to_name( name );
        ncRef.rebind(path, stationRef);

        System.out.println("Сервер готов и ждет ...");
  
        orb.run();
    }
}
