import Cell.TubeCallbackPOA;

class TubeCallbackServant extends TubeCallbackPOA {
    private String myNum;

    public TubeCallbackServant(String num) {
        myNum = num;
    }

    @Override
    public int sendSMS(String fromNum, String message) {
        System.out.println(myNum + ": принято сообщение от " + fromNum + ": " + message);
        return 0;
    }

    @Override
    public String getNum() {
        return myNum;
    }
};

public class Tube {

    private TubeCallbackServant servant;
    private String number;

    public Tube(String number) {
        this.number = number;
        this.servant = new TubeCallbackServant(number);
    }

    public String getNumber() {
        return number;
    }

    public TubeCallbackServant getServant() {
        return servant;
    }
}
