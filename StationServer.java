import Cell.Station;
import Cell.StationHelper;
import Cell.StationPOA;
import Cell.TubeCallback;
import org.omg.CosNaming.*;
import org.omg.CORBA.*;
import org.omg.PortableServer.*;
import org.omg.PortableServer.POA;

import java.util.Map;
import java.util.HashMap;


class StationServant extends StationPOA {
    private Map<String, TubeCallback> tubeMap = new HashMap<>();


    @Override
    public int register (TubeCallback objRef, String phoneNum) {
        tubeMap.put(phoneNum, objRef);

        System.out.println("Станция: зарегистрирована трубка " + phoneNum);
        return 1;
    };


    public int sendSMS (String fromNum, String toNum, String message) {
        if (!tubeMap.containsKey(toNum)) {
            System.out.println("Станция: телефон " + toNum + " не зарегистрирован");
        }

        System.out.println("Станция: трубка " + fromNum+ " посылает сообщение " + toNum);
        TubeCallback callback = tubeMap.get(toNum);
        if (callback != null) {
            callback.sendSMS(fromNum, message);
        }
        return 1;
    }
}


public class StationServer {

    public static void main(String args[]) {
        try {
            StationORBHelper helper = new StationORBHelper(args);
            helper.processStation();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
