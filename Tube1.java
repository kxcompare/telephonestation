import Cell.Station;

import java.io.*;

public class Tube1 {

    public static void main(String[] args) {
        try {
            String myNum = args[4];
            Tube tube = new Tube(myNum);
            TubeORBHelper helper = new TubeORBHelper(args, tube);
            Station station = helper.processTube();
            station.register(helper.getCallback(), tube.getNumber());
            System.out.println("Трубка с номером " + tube.getNumber() + " зарегистрирована базовой станцией");

            BufferedReader input = new BufferedReader(new InputStreamReader(System.in));
            String msg;
            while (true) {
                msg = input.readLine();
                String[] parts = msg.split(":");
                if (parts.length < 2) {
                    System.out.println("Неверно задан номер и сообщение");
                }
                String phone = parts[0].trim();
                String message = parts[1].trim();
                station.sendSMS(myNum, phone, message);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}